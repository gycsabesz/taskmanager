package aero.gobeyond.taskmanager;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;

@SpringBootApplication
@EnableScheduling
// Override the default bean name generator implementation to avoid conflict of beans with the same name (i.e.: OptionalMapperImpl)
@ComponentScan(nameGenerator = PackageBasedAnnotationBeanNameGenerator.class)
public class Application {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)

                .run(args);
    }

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(H2)
                .setScriptEncoding("UTF-8")
                .build();
    }

}
