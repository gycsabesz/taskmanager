package aero.gobeyond.taskmanager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TaskApiITest {
    private static final String TASK_JSON = """
            {
              "name": "java-task",
              "description": "task description",
              "date_time": "2022-01-01 00:00:00"
            }
            """;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql({"/test-users.sql"})
    @Transactional
    void createTaskShouldReturnWithHttpCreatedAndCreatedTask() throws Exception {
        mockMvc
                .perform(post("/user/{userId}/task", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TASK_JSON))

                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("java-task"))
                .andExpect(jsonPath("$.description").value("task description"))
                .andExpect(jsonPath("$.status").value("pending"))
                .andExpect(jsonPath("$.date_time").value("2022-01-01 00:00:00"));
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void updateTaskShouldReturnWithHttpOkAndUpdatedTask() throws Exception {
        mockMvc
                .perform(put("/user/{userId}/task/{taskId}", 1, 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TASK_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("java-task"));
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void updateUnknownTaskShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(put("/user/{userId}/task/{taskId}", 1, Long.MAX_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TASK_JSON))

                .andExpect(status().isNotFound());
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void updateTaskOfUnknownUserShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(put("/user/{userId}/task/{taskId}", Long.MAX_VALUE, 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TASK_JSON))

                .andExpect(status().isNotFound());
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void getTasksShouldReturnWithHttpOkAndExistingTasks() throws Exception {
        mockMvc
                .perform(get("/user/{userId}/task", 1))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*").value(hasSize(1)));
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void deleteTasksShouldReturnWithHttpNoContent() throws Exception {
        mockMvc
                .perform(delete("/user/{userId}/task/{taskId}", 1, 1))

                .andExpect(status().isNoContent());
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void deleteUnknownTaskShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(delete("/user/{userId}/task/{taskId}", 1, Long.MAX_VALUE))

                .andExpect(status().isNotFound());
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void deleteTaskOfUnknownUserShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(delete("/user/{userId}/task/{taskId}", Long.MAX_VALUE, 1))

                .andExpect(status().isNotFound());
    }


    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void getTasksShouldReturnWithHttpOkAndExistingTask() throws Exception {
        mockMvc
                .perform(get("/user/{userId}/task/{taskId}", 1, 1))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("task"));
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void getUnknownTaskShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(get("/user/{userId}/task/{taskId}", 1, Long.MAX_VALUE))

                .andExpect(status().isNotFound());
    }

    @Test
    @Sql({"/test-users.sql", "/test-tasks.sql"})
    @Transactional
    void getTaskOfUnknownUserShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(get("/user/{userId}/task/{taskId}", Long.MAX_VALUE, 1))

                .andExpect(status().isNotFound());
    }
}