package aero.gobeyond.taskmanager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserApiITest {
    public static final String JANE_DOE_JSON = """
            {
              "first_name": "Jane",
              "last_name": "Doe",
              "username": "jane"
            }
            """;
    private static final String JOHN_DOE_JSON = """
            {
              "first_name": "John",
              "last_name": "Doe",
              "username": "john"
            }
            """;
    @Autowired
    private MockMvc mockMvc;

    @Test
    @Transactional
    void createNewUserShouldReturnWithHttpCreatedAndCreatedUser() throws Exception {
        mockMvc
                .perform(post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JANE_DOE_JSON))

                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.first_name").value("Jane"))
                .andExpect(jsonPath("$.last_name").value("Doe"))
                .andExpect(jsonPath("$.username").value("jane"));
    }

    @Test
    @Sql({"/test-users.sql"})
    @Transactional
    void updateUserShouldReturnWithHttpOkAndUpdatedUser() throws Exception {
        mockMvc
                .perform(put("/user/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JOHN_DOE_JSON))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("john"));
    }

    @Test
    @Sql({"/test-users.sql"})
    @Transactional
    void updateUnknownUserShouldReturnWithHttpNotFound() throws Exception {
        mockMvc
                .perform(put("/user/{id}", Long.MAX_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JOHN_DOE_JSON))

                .andExpect(status().isNotFound());
    }

    @Test
    @Sql({"/test-users.sql"})
    @Transactional
    void getUsersShouldReturnWithHttpOkAndExistingUsers() throws Exception {
        mockMvc
                .perform(get("/user"))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*").value(hasSize(2)));
    }
}