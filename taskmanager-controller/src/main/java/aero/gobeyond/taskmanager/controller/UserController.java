package aero.gobeyond.taskmanager.controller;

import aero.gobeyond.taskmanager.api.UserApi;
import aero.gobeyond.taskmanager.model.User;
import aero.gobeyond.taskmanager.model.UserRequest;
import aero.gobeyond.taskmanager.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements UserApi {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<User> createUser(UserRequest userRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userService.createUser(userRequest));
    }

    @Override
    public ResponseEntity<User> getUser(Long userId) {
        return ResponseEntity.of(userService.getUser(userId));
    }

    @Override
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @Override
    public ResponseEntity<User> updateUser(Long userId, UserRequest userRequest) {
        return ResponseEntity.ok(userService.updateUser(userId, userRequest));
    }
}
