package aero.gobeyond.taskmanager.controller;

import aero.gobeyond.taskmanager.api.TaskApi;
import aero.gobeyond.taskmanager.model.Task;
import aero.gobeyond.taskmanager.model.TaskRequest;
import aero.gobeyond.taskmanager.service.TaskService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskController implements TaskApi {
    private final TaskService taskService;

    @SuppressFBWarnings("EI_EXPOSE_REP2")
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public ResponseEntity<Task> createTask(Long userId, TaskRequest taskRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(taskService.createTask(userId, taskRequest));
    }

    @Override
    public ResponseEntity<Void> deleteTask(Long userId, Long taskId) {
        taskService.deleteTask(userId, taskId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Task> getTask(Long userId, Long taskId) {
        return ResponseEntity.of(taskService.getTask(userId, taskId));
    }

    @Override
    public ResponseEntity<List<Task>> getTasks(Long userId) {
        return ResponseEntity.ok(taskService.getTasks(userId));
    }

    @Override
    public ResponseEntity<Task> updateTask(Long userId, Long taskId, TaskRequest taskRequest) {
        return ResponseEntity.ok(taskService.updateTask(userId, taskId, taskRequest));
    }
}
