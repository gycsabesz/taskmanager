# Task Manager

This is an example project for demonstrating a multi-layered *Spring Boot* application for building RESTful 
Web Services.

![Task Manager High Level Architecture](/docs/high-level-overview.png)

As you can see the DAO implementation is not relevant. For now, only for demonstration purposes, there is a trivial
JDBC implementation provided, however it could be replaced without affecting other layers of the application.

## Contract First Approach

Since I'm a big fan of the Contract First Design approach I choose to document the API requirements in form of OpenAPI 3 
(fka Swagger) specification first. *OpenAPI Generator Tool* and the corresponding maven plugin takes care about the rest and
generates server side code (i.e.: Spring Rest Controller interfaces and DTOs with the appropriate JSR303 validation annotations). 

The OpenAPI specification is the single source of truth (the contract) and might be used for generating other artifacts such as 
client side code, E2E test code, human readable API documentation, etc.

[See Task Manager OpenAPI Specification](./taskmanager-api/src/main/resources/taskmanager.openapi.yaml)

## Some API feedbacks

* I'd use ISO-8601 standardized time representation instead of custom `YYYY-MM-DD hh:mm:ss` in REST API. 
  The requested time format is not the standardized way of representing date-times in REST and, what is more
  important, it lacks to contain any time zone information, which could lead to further misunderstanding in 
  real life scenarios.
* I'd not expose the internal, auto-incrementing internal ID of the resources on REST API in order to avoid 
  tightly coupling between internal and external representations and also increase API security. Instead,
  I'd use UUID as a public-key for REST resources.

## Tech Stack

* OpenAPI 3 - API specification
* Spring Boot 2
  * Spring Data JDBC - trivial DAO implementation
* Flyway - Version control library for RDBMS databases
* Annotation Processors:
  * Mapstruct - generating type-safe converters for Value Objects and Entities 
  * Immutables - generating immutable Value Objects
* Testing libraries: Hamcrest, Junit 5, Spring Test, Mockito
* Code quality tools: PMD, Checkstyle, Spotbugs, Jacoco

## Usage

### How to start

```shell
mvn clean install
mvn spring-boot:run -pl taskmanager-app
```

### How to test

Use Postman and import the OpenAPI specification located under the `resources` of `taskmanager-api`. Change the `baseUrl`
variable from `/api` to `localhost:8080/api` and you are free to go. Execute any of the endpoints.
