package aero.gobeyond.taskmanager.dao;

import aero.gobeyond.taskmanager.dao.entity.UserEntity;

public interface UserDao extends Dao<Long, UserEntity> {
}
