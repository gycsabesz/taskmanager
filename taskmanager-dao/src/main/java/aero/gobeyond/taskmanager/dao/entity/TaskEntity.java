package aero.gobeyond.taskmanager.dao.entity;

import org.immutables.value.Value;

import java.time.OffsetDateTime;
import java.util.Optional;

@Value.Immutable
public interface TaskEntity {
    TaskEntityId getId();

    String getName();

    Optional<String> getDescription();

    TaskStatus getStatus();

    Optional<OffsetDateTime> getDueDateTime();

    enum TaskStatus {
        PENDING, DONE
    }
}
