package aero.gobeyond.taskmanager.dao.entity;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public interface TaskEntityId {
    Long getUserId();

    Optional<Long> getTaskId();
}
