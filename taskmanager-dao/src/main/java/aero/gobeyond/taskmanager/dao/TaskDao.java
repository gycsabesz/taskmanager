package aero.gobeyond.taskmanager.dao;

import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import aero.gobeyond.taskmanager.dao.entity.TaskEntityId;

import java.time.OffsetDateTime;
import java.util.List;

public interface TaskDao extends Dao<TaskEntityId, TaskEntity> {

    /**
     * {@inheritDoc}
     *
     * @throws EntityNotFoundException in case the user, the task belongs to, does not exist
     */
    @Override
    TaskEntity save(TaskEntity entity);

    /**
     * Returns all the tasks belong to the user identified by the given id.
     *
     * @param userId user id
     * @return all tasks of the user
     * @throws EntityNotFoundException in case there is no such user with the given id
     */
    List<? extends TaskEntity> findByUserId(Long userId);

    List<? extends TaskEntity> autocomplete(OffsetDateTime dueDateTime);
}
