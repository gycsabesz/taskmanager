package aero.gobeyond.taskmanager.dao.entity;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public interface UserEntity {
    Optional<Long> getId();

    String getUsername();

    String getFirstName();

    String getLastName();
}
