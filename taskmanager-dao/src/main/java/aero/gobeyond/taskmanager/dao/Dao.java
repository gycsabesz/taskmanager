package aero.gobeyond.taskmanager.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<K, T> {
    /**
     * Saves the given entity and returns with the saved entity.
     *
     * @param entity entity to be saved
     * @return saved entity
     */
    T save(T entity);

    /**
     * Deletes the entity with the given id.
     *
     * @param id entity id
     * @throws EntityNotFoundException in case there is no such entity with the given id
     */
    void delete(K id);

    Optional<? extends T> findOne(K id);

    List<? extends T> findAll();
}
