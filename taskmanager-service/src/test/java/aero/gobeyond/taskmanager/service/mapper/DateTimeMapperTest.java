package aero.gobeyond.taskmanager.service.mapper;

import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class DateTimeMapperTest {
    private static final String STRING_DATE_TIME = "2022-11-14 03:18:48";
    private static final OffsetDateTime OFFSET_DATE_TIME = OffsetDateTime.parse("2022-11-14T03:18:48Z");
    private static final OffsetDateTime OFFSET_DATE_TIME_GMT_2 = OffsetDateTime.parse("2022-11-14T05:18:48+02:00");
    private final DateTimeMapper underTest = new DateTimeMapperImpl();

    @Test
    void shouldMapStringToOffsetDateTimeInUtcZone() {
        // given

        // when
        OffsetDateTime result = underTest.fromString(STRING_DATE_TIME);

        // then
        assertThat(result, equalTo(OFFSET_DATE_TIME));
    }

    @Test
    void shouldMapOffsetDateTimeInUtcZoneToStringUtc() {
        // given

        // when
        String result = underTest.toString(OFFSET_DATE_TIME);

        // then
        assertThat(result, equalTo(STRING_DATE_TIME));
    }

    @Test
    void shouldMapOffsetDateTimeInNotUtcZoneToStringUtc() {
        // given

        // when
        String result = underTest.toString(OFFSET_DATE_TIME_GMT_2);

        // then
        assertThat(result, equalTo(STRING_DATE_TIME));
    }
}