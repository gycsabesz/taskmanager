package aero.gobeyond.taskmanager.service.mapper;

import org.mapstruct.Mapper;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Mapper
public interface DateTimeMapper {
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd HH:mm:ss");

    default OffsetDateTime fromString(String dateTime) {
        if (dateTime == null) {
            return null;
        }

        return LocalDateTime
                .parse(dateTime, DATE_TIME_FORMATTER)
                .atZone(ZoneId.of("UTC"))
                .toOffsetDateTime();
    }

    default String toString(OffsetDateTime offsetDateTime) {
        if (offsetDateTime == null) {
            return null;
        }

        return offsetDateTime
                .atZoneSameInstant(ZoneId.of("UTC"))
                .format(DATE_TIME_FORMATTER);

    }
}
