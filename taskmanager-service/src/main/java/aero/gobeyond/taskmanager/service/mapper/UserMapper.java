package aero.gobeyond.taskmanager.service.mapper;

import aero.gobeyond.taskmanager.dao.entity.UserEntity;
import aero.gobeyond.taskmanager.model.User;
import aero.gobeyond.taskmanager.model.UserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = {OptionalMapper.class, DateTimeMapper.class}, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface UserMapper {
    User toModel(UserEntity entity);

    @Mapping(target = "id", ignore = true)
    UserEntity toEntity(UserRequest model);

    UserEntity toEntity(Long id, UserRequest model);
}
