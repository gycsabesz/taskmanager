package aero.gobeyond.taskmanager.service;

import aero.gobeyond.taskmanager.dao.TaskDao;
import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;

@Component
@ConditionalOnProperty(value = "autocompletion.enabled", havingValue = "true")
public class TaskAutoCompletionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskAutoCompletionService.class);
    private final TaskDao taskDao;

    public TaskAutoCompletionService(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    @Scheduled(cron = "${autocompletion.scheduling.cron}")
    public void autoCompleteTasks() {
        OffsetDateTime now = OffsetDateTime.now();

        List<? extends TaskEntity> result = taskDao.autocomplete(now);

        LOGGER.info(
                "Auto completion performed for {} tasks because they were in pending status with due date before {}.",
                result.size(),
                now);

        if (LOGGER.isDebugEnabled()) {
            result.forEach(t -> LOGGER.debug("{}", t));
        }
    }
}
