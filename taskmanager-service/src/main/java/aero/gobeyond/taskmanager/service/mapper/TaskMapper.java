package aero.gobeyond.taskmanager.service.mapper;

import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import aero.gobeyond.taskmanager.dao.entity.TaskEntityId;
import aero.gobeyond.taskmanager.model.Task;
import aero.gobeyond.taskmanager.model.TaskRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = {OptionalMapper.class, DateTimeMapper.class}, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TaskMapper {
    @Mapping(target = "dateTime", source = "entity.dueDateTime")
    @Mapping(target = "id", source = "entity.id.taskId")
    Task toModel(TaskEntity entity);

    @Mapping(target = "dueDateTime", expression = "java( optionalMapper.wrap( dateTimeMapper.fromString( taskRequest.getDateTime() ) ) )")
    @Mapping(target = "id", expression = "java( toEntityId( userId, taskId ) )")
    @Mapping(target = "status", defaultValue = "PENDING")
    TaskEntity toEntity(Long userId, Long taskId, TaskRequest taskRequest);

    @Mapping(target = "dueDateTime", expression = "java( optionalMapper.wrap( dateTimeMapper.fromString( taskRequest.getDateTime() ) ) )")
    @Mapping(target = "id", expression = "java( toEntityId( userId ) )")
    @Mapping(target = "status", defaultValue = "PENDING")
    TaskEntity toEntity(Long userId, TaskRequest taskRequest);

    @Mapping(target = "taskId", ignore = true)
    TaskEntityId toEntityId(Long userId);

    TaskEntityId toEntityId(Long userId, Long taskId);
}
