package aero.gobeyond.taskmanager.service;

import aero.gobeyond.taskmanager.dao.UserDao;
import aero.gobeyond.taskmanager.model.User;
import aero.gobeyond.taskmanager.model.UserRequest;
import aero.gobeyond.taskmanager.service.mapper.UserMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class UserService {
    private final UserDao userDao;
    private final UserMapper userMapper;

    public UserService(UserDao userDao, UserMapper userMapper) {
        this.userDao = userDao;
        this.userMapper = userMapper;
    }

    public List<User> getUsers() {
        return userDao.findAll().stream().map(userMapper::toModel).toList();
    }

    public Optional<User> getUser(Long id) {
        return userDao.findOne(id).map(userMapper::toModel);
    }

    public User createUser(UserRequest user) {
        return userMapper.toModel(userDao.save(userMapper.toEntity(user)));
    }

    public User updateUser(Long userId, UserRequest userRequest) {
        return userMapper.toModel(userDao.save(userMapper.toEntity(userId, userRequest)));
    }
}
