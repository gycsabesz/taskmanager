package aero.gobeyond.taskmanager.service;

import aero.gobeyond.taskmanager.dao.TaskDao;
import aero.gobeyond.taskmanager.dao.entity.ImmutableTaskEntityId;
import aero.gobeyond.taskmanager.model.Task;
import aero.gobeyond.taskmanager.model.TaskRequest;
import aero.gobeyond.taskmanager.service.mapper.TaskMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskDao taskDao;
    private final TaskMapper taskMapper;

    public TaskService(TaskDao taskDao, TaskMapper taskMapper) {
        this.taskDao = taskDao;
        this.taskMapper = taskMapper;
    }

    public Optional<Task> getTask(Long userId, Long taskId) {
        return taskDao
                .findOne(ImmutableTaskEntityId.builder().userId(userId).taskId(taskId).build())
                .map(taskMapper::toModel);
    }

    public List<Task> getTasks(Long userId) {
        return taskDao.findByUserId(userId).stream().map(taskMapper::toModel).toList();
    }

    public void deleteTask(Long userId, Long taskId) {
        taskDao.delete(ImmutableTaskEntityId.builder().userId(userId).taskId(taskId).build());
    }

    public Task createTask(Long userId, TaskRequest taskRequest) {
        return taskMapper.toModel(taskDao.save(taskMapper.toEntity(userId, taskRequest)));
    }

    public Task updateTask(Long userId, Long taskId, TaskRequest taskRequest) {
        return taskMapper.toModel(taskDao.save(taskMapper.toEntity(userId, taskId, taskRequest)));
    }
}
