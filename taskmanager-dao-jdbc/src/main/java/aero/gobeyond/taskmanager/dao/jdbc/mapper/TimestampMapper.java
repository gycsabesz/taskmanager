package aero.gobeyond.taskmanager.dao.jdbc.mapper;

import org.mapstruct.Mapper;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Mapper
public interface TimestampMapper {

    default OffsetDateTime dateTimeFromJdbc(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }

        return OffsetDateTime.ofInstant(timestamp.toInstant(), ZoneId.of("UTC"));
    }

    default Timestamp dateTimeToJdbc(OffsetDateTime offsetDateTime) {
        if (offsetDateTime == null) {
            return null;
        }

        return Timestamp.from(offsetDateTime.toInstant());
    }
}
