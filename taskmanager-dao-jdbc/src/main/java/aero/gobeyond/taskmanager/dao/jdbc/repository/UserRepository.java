package aero.gobeyond.taskmanager.dao.jdbc.repository;

import aero.gobeyond.taskmanager.dao.jdbc.entity.ImmutableUserJdbcEntity;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<ImmutableUserJdbcEntity, Long> {
    @Modifying
    @Query("DELETE FROM \"users\" WHERE \"id\" = :id")
    boolean delete(Long id);
}
