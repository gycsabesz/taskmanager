package aero.gobeyond.taskmanager.dao.jdbc.repository;

import aero.gobeyond.taskmanager.dao.jdbc.entity.ImmutableTaskJdbcEntity;
import aero.gobeyond.taskmanager.dao.jdbc.entity.TaskJdbcEntity;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends CrudRepository<ImmutableTaskJdbcEntity, Long> {
    List<? extends TaskJdbcEntity> findByUserId(Long userId);

    Optional<? extends TaskJdbcEntity> findByUserIdAndTaskId(Long userId, Long taskId);

    @Modifying
    @Query("DELETE FROM \"tasks\" WHERE \"user_id\" = :userId AND \"task_id\" = :taskId")
    boolean delete(Long userId, Long taskId);

    List<? extends TaskJdbcEntity> findByStatusAndDueDateTimeBefore(Integer status, Timestamp dueDateTime);
}
