package aero.gobeyond.taskmanager.dao.jdbc;

import aero.gobeyond.taskmanager.dao.EntityNotFoundException;
import aero.gobeyond.taskmanager.dao.UserDao;
import aero.gobeyond.taskmanager.dao.entity.UserEntity;
import aero.gobeyond.taskmanager.dao.jdbc.mapper.UserEntityMapper;
import aero.gobeyond.taskmanager.dao.jdbc.repository.UserRepository;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.data.relational.core.conversion.DbActionExecutionException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Repository
public class UserJdbcDao implements UserDao {
    private final UserRepository repository;
    private final UserEntityMapper mapper;

    @SuppressFBWarnings("EI_EXPOSE_REP2")
    public UserJdbcDao(UserRepository repository,
                       UserEntityMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public UserEntity save(UserEntity entity) {
        try {
            return mapper.fromJdbc(repository.save(mapper.toJdbc(entity)));
        } catch (DbActionExecutionException e) {
            throw new EntityNotFoundException("Can not save user with id '" + entity.getId() + "'", e);
        }
    }

    @Override
    public void delete(Long id) {
        if (!repository.delete(id)) {
            throw new EntityNotFoundException("Can not delete user with id '" + id + "'");
        }
    }

    @Override
    public Optional<? extends UserEntity> findOne(Long id) {
        return repository
                .findById(id)
                .map(mapper::fromJdbc);
    }

    @Override
    public List<? extends UserEntity> findAll() {
        return StreamSupport
                .stream(repository.findAll().spliterator(), false)
                .map(mapper::fromJdbc)
                .toList();
    }
}
