package aero.gobeyond.taskmanager.dao.jdbc.mapper;

import aero.gobeyond.taskmanager.dao.entity.ImmutableUserEntity;
import aero.gobeyond.taskmanager.dao.entity.UserEntity;
import aero.gobeyond.taskmanager.dao.jdbc.entity.ImmutableUserJdbcEntity;
import aero.gobeyond.taskmanager.dao.jdbc.entity.UserJdbcEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = OptionalMapper.class, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface UserEntityMapper {

    ImmutableUserJdbcEntity toJdbc(UserEntity entity);

    ImmutableUserEntity fromJdbc(UserJdbcEntity entity);
}
