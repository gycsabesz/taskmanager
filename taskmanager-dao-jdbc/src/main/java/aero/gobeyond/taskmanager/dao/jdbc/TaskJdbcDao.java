package aero.gobeyond.taskmanager.dao.jdbc;

import aero.gobeyond.taskmanager.dao.EntityNotFoundException;
import aero.gobeyond.taskmanager.dao.TaskDao;
import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import aero.gobeyond.taskmanager.dao.entity.TaskEntityId;
import aero.gobeyond.taskmanager.dao.jdbc.entity.ImmutableTaskJdbcEntity;
import aero.gobeyond.taskmanager.dao.jdbc.mapper.TaskEntityMapper;
import aero.gobeyond.taskmanager.dao.jdbc.mapper.TimestampMapper;
import aero.gobeyond.taskmanager.dao.jdbc.repository.TaskRepository;
import aero.gobeyond.taskmanager.dao.jdbc.repository.UserRepository;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.data.relational.core.conversion.DbActionExecutionException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Repository
public class TaskJdbcDao implements TaskDao {
    private final TaskRepository repository;
    private final TaskEntityMapper mapper;

    private final TimestampMapper timestampMapper;
    private final UserRepository userRepository;

    @SuppressFBWarnings("EI_EXPOSE_REP2")
    public TaskJdbcDao(TaskRepository repository,
                       TaskEntityMapper mapper,
                       TimestampMapper timestampMapper,
                       UserRepository userRepository) {
        this.repository = repository;
        this.mapper = mapper;
        this.timestampMapper = timestampMapper;
        this.userRepository = userRepository;
    }

    @Override
    public TaskEntity save(TaskEntity entity) {
        try {
            return mapper.fromJdbc(repository.save(mapper.toJdbc(entity)));
        } catch (DbActionExecutionException e) {
            throw new EntityNotFoundException("Can not save task with id '" + entity.getId() + "'", e);
        }
    }

    @Override
    public void delete(TaskEntityId id) {
        if (!repository.delete(id.getUserId(), id.getTaskId().orElseThrow())) {
            throw new EntityNotFoundException("Can not delete task with id '" + id + "'");
        }
    }

    @Override
    public Optional<? extends TaskEntity> findOne(TaskEntityId id) {
        return repository
                .findByUserIdAndTaskId(id.getUserId(), id.getTaskId().orElseThrow())
                .map(mapper::fromJdbc);
    }

    @Override
    public List<? extends TaskEntity> findAll() {
        return StreamSupport
                .stream(repository.findAll().spliterator(), false)
                .map(mapper::fromJdbc)
                .toList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<? extends TaskEntity> findByUserId(Long userId) {
        List<TaskEntity> result = repository.findByUserId(userId)
                .stream()
                .map(mapper::fromJdbc)
                .toList();

        if (result.isEmpty()) {
            userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("Can not retrieve tasks of user with id '" + userId + "'"));
        }

        return result;
    }

    // FIXME this is only a trivial, low performant solution only for demonstration purposes
    @Override
    public List<? extends TaskEntity> autocomplete(OffsetDateTime dueDateTime) {
        Integer pendingStatus = mapper.statusToJdbc(TaskEntity.TaskStatus.PENDING);
        Integer completedStatus = mapper.statusToJdbc(TaskEntity.TaskStatus.DONE);
        Timestamp dueDateTimestamp = timestampMapper.dateTimeToJdbc(dueDateTime);

        Iterable<ImmutableTaskJdbcEntity> updateResult = repository.saveAll(
                repository
                        .findByStatusAndDueDateTimeBefore(pendingStatus, dueDateTimestamp).stream()
                        .map(t -> ImmutableTaskJdbcEntity.builder().from(t).status(completedStatus).build())
                        .toList()
        );

        return StreamSupport
                .stream(updateResult.spliterator(), false)
                .map(mapper::fromJdbc)
                .toList();
    }
}
