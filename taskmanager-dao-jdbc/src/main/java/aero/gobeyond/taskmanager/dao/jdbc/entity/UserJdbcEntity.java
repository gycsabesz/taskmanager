package aero.gobeyond.taskmanager.dao.jdbc.entity;

import org.immutables.value.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

@Value.Immutable
@Table("users")
public interface UserJdbcEntity {
    @Id
    @Column("id")
    @Nullable
    Long getId();

    @Column("username")
    String getUsername();

    @Column("first_name")
    String getFirstName();

    @Column("last_name")
    String getLastName();
}
