package aero.gobeyond.taskmanager.dao.jdbc.mapper;

import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import aero.gobeyond.taskmanager.dao.entity.TaskEntityId;
import aero.gobeyond.taskmanager.dao.jdbc.entity.ImmutableTaskJdbcEntity;
import aero.gobeyond.taskmanager.dao.jdbc.entity.TaskJdbcEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import static aero.gobeyond.taskmanager.dao.entity.TaskEntity.TaskStatus.DONE;
import static aero.gobeyond.taskmanager.dao.entity.TaskEntity.TaskStatus.PENDING;

@Mapper(uses = {OptionalMapper.class, TimestampMapper.class}, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TaskEntityMapper {

    @Mapping(target = "userId", source = "id.userId")
    @Mapping(target = "taskId", source = "id.taskId")
    ImmutableTaskJdbcEntity toJdbc(TaskEntity entity);

    @Mapping(target = "dueDateTime",
            expression = "java( optionalMapper.wrap( timestampMapper.dateTimeFromJdbc( entity.getDueDateTime() ) ) )")
    @Mapping(target = "id", expression = "java( toEntityId( entity ) )")
    TaskEntity fromJdbc(TaskJdbcEntity entity);

    @Mapping(target = "userId", source = "entity.userId")
    @Mapping(target = "taskId", source = "entity.taskId")
    TaskEntityId toEntityId(TaskJdbcEntity entity);

    default Integer statusToJdbc(TaskEntity.TaskStatus status) {
        return switch (status) {
            case PENDING -> 0;
            case DONE -> 50;
        };
    }

    default TaskEntity.TaskStatus statusFromJdbc(Integer status) {
        return switch (status) {
            case 0 -> PENDING;
            case 50 -> DONE;
            default -> throw new IllegalArgumentException("Unknown task status value: " + status);
        };
    }
}
