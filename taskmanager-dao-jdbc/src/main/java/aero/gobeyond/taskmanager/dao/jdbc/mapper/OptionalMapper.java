package aero.gobeyond.taskmanager.dao.jdbc.mapper;

import org.mapstruct.Mapper;

import java.util.Optional;

@Mapper
public interface OptionalMapper {
    default <T> T unwrap(Optional<T> optional) {
        return optional.orElse(null);
    }

    default <T> Optional<T> wrap(T value) {
        return Optional.ofNullable(value);
    }
}
