package aero.gobeyond.taskmanager.dao.jdbc.entity;

import org.immutables.value.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;

@Value.Immutable
@Table("tasks")
public interface TaskJdbcEntity {
    @Id
    @Column("task_id")
    @Nullable
    Long getTaskId();

    @Column("user_id")
    Long getUserId();

    @Column("description")
    @Nullable
    String getDescription();

    @Column("name")
    String getName();

    @Column("due_date_time")
    @Nullable
    Timestamp getDueDateTime();

    @Column("status")
    Integer getStatus();
}
