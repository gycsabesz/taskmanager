CREATE TABLE "users" (
    "id" BIGINT NOT NULL AUTO_INCREMENT,
    "first_name" VARCHAR(255) NOT NULL,
    "last_name" VARCHAR(255) NOT NULL,
    "username" VARCHAR(50) NOT NULL,

    CONSTRAINT "pk_users"
        PRIMARY KEY ("id")
);

CREATE TABLE "tasks" (
    "task_id" BIGINT NOT NULL AUTO_INCREMENT,
    "user_id" BIGINT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "description" VARCHAR(5000),
    "status" TINYINT NOT NULL,
    "due_date_time" TIMESTAMP,

    CONSTRAINT "pk_tasks"
        PRIMARY KEY ("task_id"),

    CONSTRAINT "fk_tasks_users"
        FOREIGN KEY ("user_id")
        REFERENCES "users"("id")
        ON DELETE CASCADE,

    -- representing status with integers
    --   0: PENDING
    --   50: DONE
    CONSTRAINT "chk_tasks_status"
        CHECK ("status" IN (0, 50))
);
