package aero.gobeyond.taskmanager.dao.jdbc;

import aero.gobeyond.taskmanager.dao.EntityNotFoundException;
import aero.gobeyond.taskmanager.dao.entity.ImmutableUserEntity;
import aero.gobeyond.taskmanager.dao.entity.UserEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@JdbcITest
class UserJdbcDaoITest {
    private static final UserEntity JANE_DOE = ImmutableUserEntity.builder()
            .id(1L)
            .firstName("Jane")
            .lastName("Doe")
            .username("jane")
            .build();
    public static final ImmutableUserEntity JOSEPH_DOE = ImmutableUserEntity.builder()
            .firstName("Joseph")
            .lastName("Doe")
            .username("joseph")
            .build();

    @Autowired
    private UserJdbcDao underTest;

    @Test
    void shouldReturnWithAllUsersWhenGettingAll() {
        // given

        // when
        List<? extends UserEntity> result = underTest.findAll();

        // then
        assertThat(result, hasSize(2));
    }

    @Test
    void shouldReturnWithUserWhenGettingById() {
        // given

        // when
        Optional<? extends UserEntity> result = underTest.findOne(1L);

        // then
        assertThat(result.isPresent(), is(true));
        assertThat(result.get(), equalTo(JANE_DOE));
    }

    @Test
    void shouldUpdateExistingUser() {
        // given
        final ImmutableUserEntity jenny = ImmutableUserEntity
                .copyOf(JANE_DOE)
                .withFirstName("Jenny")
                .withLastName("Eod")
                .withUsername("jenny");

        // when
        final UserEntity res = underTest.save(jenny);

        // then
        assertThat(res, equalTo(jenny));
        assertThat(underTest.findOne(1L).get(), equalTo(jenny));
    }

    @Test
    void shouldThrowWhenUpdatingMissingUser() {
        // given
        final ImmutableUserEntity jenny = ImmutableUserEntity
                .copyOf(JANE_DOE)
                .withFirstName("Jenny")
                .withId(3L);

        // when + then throws
        Assertions.assertThrows(EntityNotFoundException.class, () -> underTest.save(jenny));
    }

    @Test
    void shouldDeleteExistingUser() {
        // given

        // when
        underTest.delete(2L);

        // then
        assertThat(underTest.findOne(2L).isEmpty(), is(true));
    }

    @Test
    void shouldThrowWhenDeletingMissingUser() {
        // given

        // when + then throws
        Assertions.assertThrows(EntityNotFoundException.class, () -> underTest.delete(3L));
    }

    @Test
    void shouldInsertNewUserWhenIdNotSpecified() {
        // given

        // when
        UserEntity res = underTest.save(JOSEPH_DOE);

        // then
        assertThat(res, equalTo(JOSEPH_DOE.withId(3L)));
        assertThat(underTest.findOne(3L).get(), equalTo(res));
    }

}