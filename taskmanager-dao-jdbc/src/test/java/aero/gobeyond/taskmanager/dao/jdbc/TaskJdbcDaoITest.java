package aero.gobeyond.taskmanager.dao.jdbc;

import aero.gobeyond.taskmanager.dao.EntityNotFoundException;
import aero.gobeyond.taskmanager.dao.entity.ImmutableTaskEntity;
import aero.gobeyond.taskmanager.dao.entity.ImmutableTaskEntityId;
import aero.gobeyond.taskmanager.dao.entity.TaskEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@JdbcITest
class TaskJdbcDaoITest {
    public static final OffsetDateTime DUE_DATE_TIME = OffsetDateTime.of(2022, 12, 31, 0, 0, 0, 0, ZoneOffset.UTC);
    private static final TaskEntity JOHN_TASK_1 = ImmutableTaskEntity.builder().id(ImmutableTaskEntityId.builder().userId(1L).taskId(1L).build()).description("john_task_1_description").name("john_task_1").status(TaskEntity.TaskStatus.PENDING).dueDateTime(DUE_DATE_TIME).build();

    private static final TaskEntity JOHN_TASK_2 = ImmutableTaskEntity.builder().id(ImmutableTaskEntityId.builder().userId(1L).taskId(2L).build()).name("john_task_2").status(TaskEntity.TaskStatus.DONE).dueDateTime(DUE_DATE_TIME).build();
    private static final TaskEntity JANE_TASK_1 = ImmutableTaskEntity.builder().id(ImmutableTaskEntityId.builder().userId(2L).taskId(3L).build()).name("jane_task_1").description("jane_task_1_description").status(TaskEntity.TaskStatus.PENDING).build();
    public static final ImmutableTaskEntityId TASK_ID_WITH_NON_EXISTING_USER_ID = ImmutableTaskEntityId.builder().taskId(1L).userId(Long.MAX_VALUE).build();
    public static final ImmutableTaskEntityId TASK_ID_WITH_NON_EXISTING_TASK_ID = ImmutableTaskEntityId.builder().taskId(Long.MAX_VALUE).userId(1L).build();

    @Autowired
    private TaskJdbcDao underTest;

    @Test
    void shouldReturnAllTasks() {
        // given

        // when
        List<? extends TaskEntity> result = underTest.findAll();

        // then
        assertThat(result, hasSize(3));

        assertThat(result.get(0), equalTo(JOHN_TASK_1));
        assertThat(result.get(1), equalTo(JOHN_TASK_2));
        assertThat(result.get(2), equalTo(JANE_TASK_1));
    }

    @Test
    void shouldReturnUserTasks() {
        // given

        // when
        List<? extends TaskEntity> result = underTest.findByUserId(1L);

        // then
        assertThat(result, hasSize(2));

        assertThat(result.get(0), equalTo(JOHN_TASK_1));
        assertThat(result.get(1), equalTo(JOHN_TASK_2));
    }

    @Test
    void shouldThrowWhenRetrievingUserTasksForNonExistingUser() {
        // given

        // when + then throws
        Assertions.assertThrows(EntityNotFoundException.class, () -> underTest.findByUserId(Long.MAX_VALUE));
    }

    @Test
    void shouldReturnTask() {
        // given
        ImmutableTaskEntityId id = ImmutableTaskEntityId.builder().taskId(1L).userId(1L).build();

        // when
        Optional<? extends TaskEntity> result = underTest.findOne(id);

        // then
        assertThat(result.isPresent(), is(true));
        assertThat(result.get(), equalTo(JOHN_TASK_1));
    }

    @Test
    void shouldReturnNoTaskWhenUserIdDoesNotMatch() {
        // given

        // when
        Optional<? extends TaskEntity> result = underTest.findOne(TASK_ID_WITH_NON_EXISTING_USER_ID);

        // then
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    void shouldReturnNoTaskWhenTaskIdDoesNotMatch() {
        // given

        // when
        Optional<? extends TaskEntity> result = underTest.findOne(TASK_ID_WITH_NON_EXISTING_TASK_ID);

        // then
        assertThat(result.isEmpty(), is(true));
    }


    @Test
    void deleteShouldThrowWhenTaskIdDoesNotMatch() {
        // given

        // when + then throws
        Assertions.assertThrows(EntityNotFoundException.class, () -> underTest.delete(TASK_ID_WITH_NON_EXISTING_TASK_ID));

    }

    @Test
    void deleteShouldThrowWhenUserIdDoesNotMatch() {
        // given

        // when + then throws
        Assertions.assertThrows(EntityNotFoundException.class, () -> underTest.delete(TASK_ID_WITH_NON_EXISTING_USER_ID));
    }

    @Test
    void shouldDeleteTask() {
        // given
        ImmutableTaskEntityId id = ImmutableTaskEntityId.builder().taskId(1L).userId(1L).build();

        // when
        underTest.delete(id);

        // then
        assertThat(underTest.findByUserId(1L), hasSize(1));
    }

    @Test
    void shouldUpdateTask() {
        // given
        ImmutableTaskEntity updatedTask = ImmutableTaskEntity.copyOf(JANE_TASK_1).withDescription(Optional.empty()).withName("updated-name").withDueDateTime(OffsetDateTime.of(2022, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault().getRules().getOffset(Instant.now()))).withStatus(TaskEntity.TaskStatus.DONE);

        // when
        TaskEntity result = underTest.save(updatedTask);

        // then
        assertThat(result, is(notNullValue()));

        TaskEntity actual = underTest.findOne(updatedTask.getId()).orElseThrow();

        assertThat(actual.getId(), equalTo(updatedTask.getId()));
        assertThat(actual.getDescription(), equalTo(updatedTask.getDescription()));
        assertThat(actual.getName(), equalTo(updatedTask.getName()));
        assertThat(actual.getStatus(), equalTo(updatedTask.getStatus()));
        assertThat(actual.getDueDateTime().orElseThrow().toEpochSecond(), equalTo(updatedTask.getDueDateTime().orElseThrow().toEpochSecond()));
    }

    @Test
    void shouldThrowWhenUpdatingTaskWithNonExistingUserId() {
        // given
        ImmutableTaskEntity updatedTask = ImmutableTaskEntity.copyOf(JANE_TASK_1).withId(TASK_ID_WITH_NON_EXISTING_USER_ID);

        // when + then throws
        Assertions.assertThrows(RuntimeException.class, () -> underTest.save(updatedTask));
    }

    @Test
    void shouldThrowWhenUpdatingTaskWithNonExistingTaskId() {
        // given
        ImmutableTaskEntity updatedTask = ImmutableTaskEntity.copyOf(JANE_TASK_1).withId(TASK_ID_WITH_NON_EXISTING_TASK_ID);

        // when + then throws
        Assertions.assertThrows(RuntimeException.class, () -> underTest.save(updatedTask));
    }

    @Test
    void shouldInsertWhenTaskIdIsNotSpecified() {
        // given
        ImmutableTaskEntity taskToCreate = ImmutableTaskEntity.copyOf(JOHN_TASK_1).withId(ImmutableTaskEntityId.builder().userId(1L).build());

        // when
        TaskEntity result = underTest.save(taskToCreate);

        // then
        assertThat(result, equalTo(taskToCreate.withId(ImmutableTaskEntityId.builder().userId(1L).taskId(4L).build())));
    }
}