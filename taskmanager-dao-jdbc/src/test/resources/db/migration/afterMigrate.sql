INSERT INTO
  "users" ("username", "first_name", "last_name")
VALUES
  ('jane', 'Jane', 'Doe'),
  ('john', 'John', 'Doe');

INSERT INTO
  "tasks" ("user_id", "name", "description", "due_date_time", "status")
VALUES
  (1, 'john_task_1', 'john_task_1_description', '2022-12-31T00:00:00Z', 0),
  (1, 'john_task_2', NULL, '2022-12-31T00:00:00Z', 50),
  (2, 'jane_task_1', 'jane_task_1_description', NULL, 0);
